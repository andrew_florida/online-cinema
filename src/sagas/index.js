import { takeEvery } from 'redux-saga/effects'

import { fetchData } from '../actions'
import { FETCHED_DATA } from '../actions'

export default function* watchFetchData() {
    yield takeEvery(FETCHED_DATA, fetchData);
}