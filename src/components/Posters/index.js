import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CircularProgress from '@material-ui/core/CircularProgress';

import SinglePosterInGrid from '../SinglePosterInGrid'
import { itemsFetchData } from '../../actions';

const mapStateToProps = (state) => {
    // let test = {
    //     title: `test`, 
    //     id: `1`, 
    //     release_date: `1990-05-12`, 
    //     popularity: `7`, 
    //     poster_path: `https://image.tmdb.org/t/p/w185_and_h278_bestv2/adw6Lq9FiC9zjYEpOqfq03ituwp.jpg`, 
    //     overview: `test test test`
    // }
    return {
        items: state.items,
        itemsHasError: state.itemsHasError,
        itemsIsLoading: state.itemsIsLoading,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        ...bindActionCreators({ itemsFetchData }, dispatch)
    };
};

export class Posters extends Component {
    componentDidMount() {
        this.props.itemsFetchData();
    }

    render() {
        if (this.props.itemsHasError) {
            return <p>Sorry! There was an error loading the items</p>;
        }

        if (this.props.itemsIsLoading) {
            return <CircularProgress />
        }
        return (
            <>
                {/* {this.props.items.map(item => (
                    <SinglePosterInGrid
                        title={item.title}
                        key={item.id}
                        id={item.id}
                        release_date={item.release_date}
                        popularity={item.popularity}
                        poster_path={item.poster_path}
                        overview={item.overview}
                    />
                ))} */}
                <SinglePosterInGrid
                    title={this.props.items.title}
                    key={this.props.items.id}
                    id={this.props.items.id}
                    release_date={this.props.items.release_date}
                    popularity={this.props.items.popularity}
                    poster_path={this.props.items.poster_path}
                    overview={this.props.items.overview}
                />
            </>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Posters);

