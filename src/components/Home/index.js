import React, { Component } from 'react';

import Header from '../Header'
import Posters from '../Posters'

class Home extends Component {
    render() {
        return (
            <>
                <Header />
                <Posters />
            </>
        );
    }
}

export default Home;
