import React, { Component } from 'react';
import SearchForm from '../SearchForm';
import GenreFilter from '../GenreFilter';

class Header extends Component {
    render() {
        return (
            <>
                <h1>Welcome in cinema world</h1>
                <SearchForm />
                <GenreFilter />
            </>
        );
    }
}

export default Header;