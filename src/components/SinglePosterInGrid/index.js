//import React, { Component } from 'react';
import React, { Component } from 'react';
import { NavLink } from "react-router-dom";

class SinglePosterInGrid extends Component {
    render() {
        return(
        <div className="item poster card">
            <div className="image_content">
                <NavLink to={this.props.id} id="movie_287947" className="result" title={this.props.title} alt={this.props.title}>


                    <img className="poster fade lazyautosizes lazyloaded" data-sizes="auto" src={this.props.poster_path} alt={this.props.title} />
                    <div className="meta" data-role="tooltip">
                        <span id="popularity_53f607090e0a267f8d0055c8_value" className="hide popularity_rank_value">
                            <div className="tooltip_popup popularity">
                                <h3>Popularity Rank</h3>
                                <p>{this.props.popularity}</p>
                            </div>
                        </span>
                        <span id="popularity_53f607090e0a267f8d0055c8" className="glyphicons glyphicons-cardio x1 popularity_rank"></span>
                        <span className="right">

                        </span>
                    </div>
                </NavLink>
            </div>
            <div className="info">
                <div className="wrapper">
                    <div className="consensus tight">
                        <div className="outer_ring">
                            <div className="user_score_chart 53f607090e0a267f8d0055c8" data-percent="74.0" data-track-color="#204529" data-bar-color="#21d07a">
                                <div className="percent">

                                    <span className="icon icon-r74"></span>

                                </div>
                                <canvas height="32" width="32"></canvas></div>
                        </div>
                    </div>

                    <div className="flex">
                        <NavLink className="title result" to={this.props.id} title={this.props.title} alt={this.props.title}>{this.props.title}</NavLink>
                        <span>{this.props.release_date}</span>
                    </div>
                </div>
                <p className="overview">{this.props.overview}</p>
                <p className="view_more"><NavLink className="result" to={this.props.id} title={this.props.title} alt={this.props.title}>More Info</NavLink></p>
            </div>
        </div>
        )
    }
}

SinglePosterInGrid.defaultProps = {
    title: `{ item.title }`,
    key: 7,
    id: `7`,
    release_date: `2222-22-22`,
    popularity: 7.77,
    poster_path: `https://image.tmdb.org/t/p/w185_and_h278_bestv2/adw6Lq9FiC9zjYEpOqfq03ituwp.jpg`,
    overview: `test test`
};


export default SinglePosterInGrid;