import React, { Component } from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

class GenreFilter extends Component {
    render() {
        return (
            <>
                <h2>Genre</h2>
                <FormGroup row>
                    <FormControlLabel
                        control={
                            <Checkbox
                                value="checkedA"
                            />
                        }
                        label="Comedy"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                value="checkedB"
                                color="primary"
                            />
                        }
                        label="Horror"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                value="checkedC"
                            />
                        }
                        label="Fiction"
                    />

                </FormGroup>
            </>
        );
    }
}

export default GenreFilter;