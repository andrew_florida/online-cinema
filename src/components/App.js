/** https://www.themoviedb.org/settings/api*/
import React, { Component } from 'react';
import {
  Route,
  BrowserRouter as Router,
  Switch
} from "react-router-dom";
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';

//import { itemsFetchData} from '../actions';
import SinglePoster from './SinglePoster';
import Home from './Home'
import './App.css';

// const mapStateToProps = (state) => {
//   return {
//     items: state.items,
//     itemsHasError: state.itemsHasError,
//     itemsIsLoading: state.itemsIsLoading,
//   };
// };

// const mapDispatchToProps = (dispatch) => {
//   return {
//     ...bindActionCreators({ itemsFetchData}, dispatch)
//   };
// };

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Router >
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path={`/550`} component={SinglePoster} />
          </Switch>
        </Router >
      </div>
    );
  }
}

//export default connect(mapStateToProps, mapDispatchToProps)(App);