import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';

class SearchForm extends Component {
    render() {

        return (
            <>
                <FormControl>
                    <InputLabel htmlFor="component-simple">
                        Search for a movie...</InputLabel>
                    <Input id="component-simple" />
                </FormControl>

            </>
        );
    }
}

export default SearchForm;