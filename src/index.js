import React from 'react';
import ReactDOM from 'react-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux';

//import { itemsFetchData, onEdit, onChange, onDelete } from './actions';
import { store } from './store/configurateStore';
import App from './components/App';

ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider>
            <App/>
        </MuiThemeProvider>
    </Provider>, document.getElementById('root'));