import * as actions from '../actions'

export const itemsHasError = (state = false, action) => {
    switch (action.type) {
        case actions.ITEMS_HAS_ERROR:
            return action.itemsHasError;

        default:
            return state;
    }
}

export const itemsIsLoading = (state = false, action) => {
    switch (action.type) {
        case actions.ITEMS_IS_LOADING:
            return action.itemsIsLoading;

        default:
            return state;
    }
}

export const items = (items = [], action) => {
    switch (action.type) {
        case actions.ITEMS_FETCH_DATA_SUCCESS:
            return action.items;

        case actions.SORT_POPULARITY:
            return action.items;
            
        case actions.FILTER_TITLE:
            return action.items;
            
        case actions.FILTER_GENRE:
            return action.items;    

        default:
            return items
    }
}