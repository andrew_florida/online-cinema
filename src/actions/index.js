import { put, call } from 'redux-saga/effects'

export const ITEMS_IS_LOADING = 'ITEMS_IS_LOADING'
export const ITEMS_FETCH_DATA_SUCCESS = 'ITEMS_FETCH_DATA_SUCCESS'
export const ITEMS_HAS_ERROR = 'ITEMS_HAS_ERROR'
export const FETCHED_DATA = 'FETCHED_DATA'
export const SORT_POPULARITY = 'SORT_POPULARITY'
export const FILTER_TITLE = 'FILTER_TITLE'
export const FILTER_GENRE = 'FILTER_GENRE'
//../src/service/themoviedb.json /home/lenovo/Desktop/react-app/movie/online-cinema/src/service/themoviedb.json
//https://api.themoviedb.org/3/movie/555?api_key=7cef8e601d64eeb3e03fd55a1b45a8dc
let api = (url) => fetch(url).then(res => res.json())

export function* fetchData(action) {
    try {
        yield put(itemsIsLoading(true));
        const items = yield call(api, 'https://api.themoviedb.org/3/movie/555?api_key=7cef8e601d64eeb3e03fd55a1b45a8dc');
        if (items && items.length > 0) {
            yield put(itemsIsLoading(false));
            yield put(itemsFetchDataSuccess(items));
        }
    }
    catch (e) {
        yield put(itemsIsLoading(false));
        yield put(itemsHasError(true));
        console.log(e)
    }
}

export const itemsFetchData = () => ({
    type: FETCHED_DATA
});

export const itemsHasError = bool => ({
    type: ITEMS_HAS_ERROR,
    itemsHasError: bool
})

export const itemsIsLoading = bool => ({
    type: ITEMS_IS_LOADING,
    itemsIsLoading: bool
})

export const itemsFetchDataSuccess = items => ({
    type: ITEMS_FETCH_DATA_SUCCESS,
    items
})

export const sortPopularity = bool => ({
    type: SORT_POPULARITY,
    desk: bool
})

export const filterTitle = items => ({
    type: FILTER_TITLE,
    items
})

export const filterGenre = genres => ({
    type: FILTER_GENRE,
    genres
})